﻿<%@ Page Title="QR Clock" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="QR_Clock.About" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<meta http-equiv="refresh" content="30" /> 
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<center><h2><%= GetDateString() %></h2>
<img src="http://chart.apis.google.com/chart?chs=400x400&cht=qr&chld=L|0&chl=<%= GetDateString() %>" alt="<%= GetDateString() %>" /><br />
Page Auto Refreshes every 30 Seconds....<br />
This site, including domain, is for sale. <a href="mailto:tiernan [at] tiernanotoole [dot] ie">Email me</a> for details<br/>
<a href="http://twitter.com/tiernano">Follow me on Twitter</a><br/>

</center>
</asp:Content>
